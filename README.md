# Api Testing Assignment

The project was automated inside the GitLab CI. <br>
The code is inside the Gitlab repository, and can be clone to local computer.<br>
Once push the updated code to the remote repository, the pipeline will be triggered automatically using Docker, and
the Test Report are accessible under the GitLab CI/CD Pipelines > Tests. <br>
If there is any failed test scenario, the System output (expected and actual result) for particular text scenario is available.

(https://gitlab.com/yasarenver/apitestingassignment)


You can see the endpoints interactions below;

**Test Scenarios**

**Scenario:**  Call top_rated GET endpoint with Valid Api_key<br>
&nbsp; **Given** I set the valid api_key param<br>
&nbsp; **When** I call top_rated GET endpoint<br>
&nbsp; **Then** the statusCode should be 200<br>
&nbsp; **And** the response ContentType should be Json<br>
&nbsp; **Then** the default page in the body should be 1<br>
&nbsp; **And** the results, total_pages and the total_results should not be null<br><br>

**Scenario:**  Call top_rated GET endpoint with Valid Api_key and Page param<br>
&nbsp; **Given** I set the valid api_key param and page param as 3<br>
&nbsp; **When** I call top_rated GET endpoint<br>
&nbsp; **Then** the statusCode should be 200<br>
&nbsp; **And** the response ContentType should be Json<br>
&nbsp; **Then** the default page in the body should be 3<br>
&nbsp; **And** the results, total_pages and the total_results should not be null <br><br>

**Scenario:**  Call top_rated GET endpoint with Invalid Api_key<br>
&nbsp; **Given** I set an Invalid api_key param<br>
&nbsp; **When** I call top_rated GET endpoint<br>
&nbsp; **Then** the statusCode should be 401<br>
&nbsp; **And** the response ContentType should be Json<br>
&nbsp; **Then** the status_code in the response body should be 7<br>
&nbsp; **And** the status_message in the body should be "Invalid API key: You must be granted a valid key."<br><br>


**Scenario:**  Call top_rated GET endpoint with Valid Auth<br>
&nbsp; **Given** I set the Valid Auth<br>
&nbsp; **When** I call top_rated GET endpoint<br>
&nbsp; **Then** the statusCode should be 200<br>
&nbsp; **And** the response ContentType should be Json<br>
&nbsp; **Then** the default page in the body should be 1<br>
&nbsp; **And** the results, total_pages and the total_results should not be null<br><br>


**Scenario:**  Call top_rated GET endpoint with Invalid Auth<br>
&nbsp; **Given** I set an Invalid Auth<br>
&nbsp; **When** I call top_rated GET endpoint<br>
&nbsp; **Then** the statusCode should be 401<br>
&nbsp; **And** the response ContentType should be Json<br>
&nbsp; **Then** the status_code in the response body should be 7<br>
&nbsp; **And** the status_message in the body should be  "Invalid API key: You must be granted a valid key."<br><br>


**Scenario:**  Call movie rating POST endpoint with Valid Api_key and SessionID<br>
&nbsp; **Given** I set the Valid Api_key and Valid Session_Id param<br>
&nbsp; **When** I call the rating POST endpoint<br>
&nbsp; **Then** the statusCode should be 201<br>
&nbsp; **And** the response ContentType should be Json<br>
&nbsp; **Then** the status_code in the response body should be 12<br>
&nbsp; **And** the status_message in the response body should be "The item/record was updated successfully."<br><br>

**Scenario:**  Call movie rating POST endpoint with Invalid SessionID<br>
&nbsp; **Given** I set an Invalid Session_Id param<br>
&nbsp; **When** I call the rating POST endpoint<br>
&nbsp; **Then** the statusCode should be 401<br>
&nbsp; **And** the response ContentType should be Json<br>
&nbsp; **Then** the status_code in the response body should be 3<br>
&nbsp; **And** the status_message in the response body should be "Authentication failed: You do not have permissions to access the service."<br><br>


**Scenario:**  Call movie rating POST endpoint with Invalid Api_key<br>
&nbsp; **Given** I set an Invalid Api_key param<br>
&nbsp; **When** I call the rating POST endpoint<br>
&nbsp; **Then** the statusCode should be 401<br>
&nbsp; **And** the response ContentType should be Json<br>
&nbsp; **Then** the status_code in the response body should be 3<br>
&nbsp; **And** the status_message in the response body should be "Authentication failed: You do not have permissions to access the service."<br><br>

**Scenario:**  Call movie rating POST endpoint without Api_key<br>
&nbsp; **Given** I set the Api_key param as empty<br>
&nbsp; **When** I call the rating POST endpoint<br>
&nbsp; **Then** the statusCode should be 401<br>
&nbsp; **And** the response ContentType should be Json<br>
&nbsp; **Then** the status_code in the response body should be 3<br>
&nbsp; **And** the status_message in the response body should be "Authentication failed: You do not have permissions to access the service."<br><br>


**Scenario:**  Call movie rating POST endpoint without Session_Id<br>
&nbsp; **Given** I set the Session_Id param as empty<br>
&nbsp; **When** I call the rating POST endpoint<br>
&nbsp; **Then** the statusCode should be 401<br>
&nbsp; **And** the response ContentType should be Json<br>
&nbsp; **Then** the status_code in the response body should be 3<br>
&nbsp; **And** the status_message in the response body should be "Authentication failed: You do not have permissions to access the service."<br><br>

