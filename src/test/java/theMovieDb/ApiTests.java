package theMovieDb;

import utilities.ConfigurationReader;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.*;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.* ;
import static org.hamcrest.Matchers.* ;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ApiTests {

    @BeforeAll
    public static void setup(){
        RestAssured.baseURI = ConfigurationReader.getProperty("baseURI");
        RestAssured.basePath = ConfigurationReader.getProperty("basePath");
    }

    @DisplayName("Call top_rated GET endpoint with Valid Api_key")
    @Test
    public void CallTopRatedGETendpointWithValidApiKey(){
        given()
                .accept(ContentType.JSON).
                when()
                .param("api_key", ConfigurationReader.getProperty("api_key"))
                .get("/top_rated").
                then()
                .statusCode(200)
                .contentType(ContentType.JSON).
                assertThat()
                .body("page", equalTo(1))
                .body("results", notNullValue())
                .body("total_pages", notNullValue())
                .body("results.backdrop_path[0]", containsString(".jpg"))
                .body("total_results", notNullValue());

    }

    @DisplayName("Call top_rated GET endpoint with Valid Api_key and Page param")
    @Test
    public void CallTopRatedGETendpointWithValidApiKeyAndPageParam(){
        given()
                .accept(ContentType.JSON).
                when()
                .param("api_key", ConfigurationReader.getProperty("api_key"))
                .param("page", 3)
                .get("/top_rated").
                then()
                .statusCode(200)
                .contentType(ContentType.JSON).
                assertThat()
                .body("page", equalTo(3))
                .body("results", notNullValue())
                .body("total_pages", notNullValue())
                .body("results.backdrop_path[0]", containsString(".jpg"))
                .body("total_results", notNullValue())
        ;
    }

    @DisplayName("Call top_rated GET endpoint with Invalid Api_key")
    @Test
    public void CallTopRatedGETendpointWithInvalidApiKey(){
        given()
                .accept(ContentType.JSON).
                when()
                .param("api_key", "invalidApiKey")
                .get("/top_rated").
                then()
                .statusCode(401)
                .contentType(ContentType.JSON).
                assertThat()
                .body("status_code", equalTo(7))
                .body("status_message", equalTo("Invalid API key: You must be granted a valid key."))
                .body("success", equalTo(false))
        ;
    }

    @DisplayName("Call top_rated GET endpoint with Valid Auth")
    @Test
    public void CallTopRatedGETendpointWithValidAuth(){
        given()
                .accept(ContentType.JSON).
                when()
                .auth().oauth2(ConfigurationReader.getProperty("token"))
                .get("/top_rated").
                then()
                .statusCode(200)
                .contentType(ContentType.JSON).
                assertThat()
                .body("page", equalTo(1))
                .body("results", notNullValue())
                .body("total_pages", notNullValue())
                .body("results.backdrop_path[0]", containsString(".jpg"))
                .body("total_results", notNullValue())
        ;
    }

    @DisplayName("Call top_rated GET endpoint with Invalid Auth")
    @Test
    public void CallTopRatedGETendpointWithInvalidAuth(){
        given()
                .accept(ContentType.JSON).
                when()
                .auth().oauth2("invalidAuth")
                .get("/top_rated").
                then()
                .statusCode(401)
                .contentType(ContentType.JSON).
                assertThat()
                .body("status_code", equalTo(7))
                .body("status_message", equalTo("Invalid API key: You must be granted a valid key."))
                .body("success", equalTo(false))
        ;
    }
    @DisplayName("Call movie rating POST endpoint with Valid Api_key and SessionID")
    @Test
    public void  CallMovieRatingPOSTendpointWithValidApiKeyAndSessionID(){

        Map<String,Object> movieMap = new HashMap<>();
        movieMap.put("value" , 8);

        given()
                .contentType(ContentType.JSON)
                .queryParam("api_key", ConfigurationReader.getProperty("api_key"))
                .queryParam("session_id", ConfigurationReader.getProperty("session_id"))
                .body(movieMap)
                .accept(ContentType.JSON).
                when()
                .post("/637/rating").
                then()
                .statusCode(201)
                .contentType(ContentType.JSON).
                assertThat()
                .body("success", equalTo(true))
                .body("status_code", equalTo(12))
                .body("status_message", equalTo("The item/record was updated successfully."))
        ;
    }

    @DisplayName("Call movie rating POST endpoint with Invalid SessionID")
    @Test
    public void CallMovieRatingPOSTendpointWithInvalidSessionID(){

        Map<String,Object> movieMap = new HashMap<>();
        movieMap.put("value" , 8);

        given()
                .contentType(ContentType.JSON)
                .queryParam("api_key", ConfigurationReader.getProperty("api_key"))
                .queryParam("session_id", "invalidSessionId")
                .body(movieMap)
                .accept(ContentType.JSON).
                when()
                .post("/637/rating").
                then()
                .statusCode(401)
                .contentType(ContentType.JSON).
                assertThat()
                .body("success", equalTo(false))
                .body("status_code", equalTo(3))
                .body("status_message", equalTo("Authentication failed: You do not have permissions to access the service."))
        ;
    }

    @DisplayName("Call movie rating POST endpoint with Invalid Api_key")
    @Test
    public void CallMovieRatingPOSTendpointWithInvalidApiKey(){

        Map<String,Object> movieMap = new HashMap<>();
        movieMap.put("value" , 8);

        given()
                .contentType(ContentType.JSON)
                .queryParam("api_key", "invalidApiKey")
                .queryParam("session_id", ConfigurationReader.getProperty("session_id"))
                .body(movieMap)
                .accept(ContentType.JSON).
                when()
                .post("/637/rating").
                then()
                .statusCode(401)
                .contentType(ContentType.JSON).
                assertThat()
                .body("status_code", equalTo(7))
                .body("status_message", equalTo("Invalid API key: You must be granted a valid key."))
                .body("success", equalTo(false));
    }

    @DisplayName("Call movie rating POST endpoint without Api_key")
    @Test
    public void CallMovieRatingPOSTendpointWithoutApiKey(){

        Map<String,Object> movieMap = new HashMap<>();
        movieMap.put("value" , 8);

        given()
                .contentType(ContentType.JSON)
                .queryParam("session_id", ConfigurationReader.getProperty("session_id"))
                .body(movieMap)
                .accept(ContentType.JSON).
                when()
                .post("/637/rating").
                then()
                .statusCode(401)
                .contentType(ContentType.JSON).
                assertThat()
                .body("status_code", equalTo(7))
                .body("status_message", equalTo("Invalid API key: You must be granted a valid key."))
                .body("success", equalTo(false));
    }

    @DisplayName("Call movie rating POST endpoint without Session_Id")
    @Test
    public void CallMovieRatingPOSTendpointWithoutSessionId(){

        Map<String,Object> movieMap = new HashMap<>();
        movieMap.put("value" , 8);

        given()
                .contentType(ContentType.JSON)
                .queryParam("api_key", ConfigurationReader.getProperty("api_key"))
                .body(movieMap)
                .accept(ContentType.JSON).
                when()
                .post("/637/rating").
                then()
                .statusCode(401)
                .contentType(ContentType.JSON).
                assertThat()
                .body("status_code", equalTo(3))
                .body("status_message", equalTo("Authentication failed: You do not have permissions to access the service."))
                .body("success", equalTo(false));
    }


}
